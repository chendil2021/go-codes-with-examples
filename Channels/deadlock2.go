package main

import "fmt"

func show() {
	fmt.Println("Hello world")
}
func main() {
	str_ch := make(chan string)
	go show()
	a := <-str_ch
	fmt.Println("Receive operation is getting blocked", a)
}
