package main

import "fmt"

func call_by_value(x int, y int) {
	x = x - 20
	y = y - 30
	fmt.Printf("In function, value of a : %d\n", x) //prints 80
	fmt.Printf("In function, value of b : %d\n", y) //prints 170
}
func main() {
	var a int = 100
	var b int = 200
	fmt.Printf("Before calling function, value of a : %d\n", a) //prints 100
	fmt.Printf("Before calling function, value of b : %d\n", b) //prints 200
	call_by_value(a, b)
	fmt.Printf("After calling function, value of a : %d\n", a) //prints 100
	fmt.Printf("After calling function, value of b : %d\n", b) //prints 200
}
