package main

import "fmt"

func main() {
	slice1 := []int{12, 65, 34, 78, 23, 67}
	fmt.Println("capacity of slice1:", cap(slice1))
	slice2 := make([]int, len(slice1), (cap(slice1)+1)*2)
	for i := range slice1 {
		slice2[i] = slice1[i]
	}
	slice1 = slice2
	fmt.Println("capacity of slice1:", cap(slice1))
}
