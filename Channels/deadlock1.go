package main

import "fmt"

func show() {
	fmt.Println("Hello")
}
func main() {
	str_ch := make(chan string)
	go show()
	str_ch <- "Hello world"
	fmt.Println("Send operation is getting blocked")
}
