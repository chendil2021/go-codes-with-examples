package main

import (
	"fmt"
	"sync"
	"time"
)

func show_value(i int, wg *sync.WaitGroup) {
	for i < 10 {
		fmt.Println(i)
		time.Sleep(200 * time.Millisecond)
		i = i + 1
	}
	wg.Done()
}
func show_value1(c int, wg *sync.WaitGroup) {
	for c < 110 {
		fmt.Println(c)
		time.Sleep(200 * time.Millisecond)
		c = c + 1
	}
	wg.Done()
}
func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	go show_value(0, &wg)
	fmt.Println("We have called a goroutine show_value")
	go show_value1(100, &wg)
	fmt.Println("We have called a goroutine show_value1")
	wg.Wait()
	fmt.Println("Done")
}
