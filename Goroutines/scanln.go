package main

import "fmt"

func show_value(i int) {
	for i < 10 {
		fmt.Println(i)
		i = i + 1
	}
}
func main() {
	go show_value(0)
	fmt.Println("We have called the function")
	fmt.Scanln()
}
