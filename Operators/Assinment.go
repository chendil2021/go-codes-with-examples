package main

import "fmt"

func main() {
	var a int = 50
	var b int = 5
	a += b
	fmt.Println("ex. of '+=' operator:", a) //prints ex. of '+=' operator: 55
	a -= b
	fmt.Println("ex. of '-=' operator:", a) //prints ex. of '-=' operator: 50
	a *= 3
	fmt.Println("ex. of '*=' operator:", a) //prints ex. of '*=' operator: 150
	a /= b
	fmt.Println("ex. of '/=' operator:", a) //prints ex. of '/=' operator: 30
	a %= 7
	fmt.Println("ex. of '%=' operator:", a) //prints ex. of '%=' operator: 2
	b <<= 2
	fmt.Println("ex. of '<<=' operator:", b) //prints ex. of '<<=' operator: 20
	b >>= 2
	fmt.Println("ex. of '>>=' operator:", b) //prints ex. of '>>=' operator: 5
	a &= 10
	fmt.Println("ex. of '&=' operator:", a) //prints ex. of '&=' operator: 2
	a ^= 10
	fmt.Println("ex. of '^=' operator:", a) //prints ex. of '^=' operator: 8
	a |= 10
	fmt.Println("ex. of '|=' operator:", a) //prints ex. of '|=' operator: 10
	c := "hello world"
	fmt.Println("ex. of ':=' operator:", c) //prints ex. of ':=' operator: hello world
}
