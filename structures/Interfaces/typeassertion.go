package main

import "fmt"

type AirthOperation interface {
	Add() float32
}
type Operandset1 struct {
	op1 float32
	op2 float32
}
type Operandset2 struct {
	op1 float32
	op2 float32
}

func (operandset1_var Operandset1) Add() float32 {
	return operandset1_var.op1 + operandset1_var.op2
}
func main() {
	var interface_var AirthOperation
	interface_var = Operandset1{3.6, 6.9}
	t, ok := interface_var.(Operandset1)
	fmt.Println(t, ok) // {3.6 6.9} true
}
