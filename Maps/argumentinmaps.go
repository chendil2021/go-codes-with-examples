package main

import "fmt"

func change_value(phone_dict1 map[string]int64) {
	phone_dict1["Mayank"] = 1234567890
}
func main() {
	phone_dict := make(map[string]int64)
	phone_dict["Rahul"] = 9876543201
	phone_dict["Swati"] = 9123456789
	phone_dict["Suman"] = 9675463768
	phone_dict["Mayank"] = 9342516789
	phone_dict["Deepak"] = 8765453678
	fmt.Println("Before calling function, value of key 'Mayank' is:", phone_dict["Mayank"])
	change_value(phone_dict)
	fmt.Println("After calling function, value of key 'Mayank' is:", phone_dict["Mayank"])
}
