package main

import (
	"fmt"
	"time"
)

func show_value(i int) {
	for i < 10 {
		fmt.Println(i)
		time.Sleep(200 * time.Millisecond)
		i = i + 1
	}
}
func main() {
	go show_value(0)
	fmt.Println("We have called a goroutine")
	show_value(0)
	fmt.Println("We have called a function")
}
