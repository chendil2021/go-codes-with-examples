package main

import "fmt"

func main() {
	var a int = 100
	var b int = 20
	if a == b {
		fmt.Println("a is equal to b")
	} else if a%b == 0 {
		fmt.Println("a is divisible by b")
	} else {
		fmt.Println("no match")
	}
}
