package main

import "fmt"

func main() {
	arr1 := [4]int{1, 4, 7, 2}
	arr2 := arr1
	arr2[2] = 90
	fmt.Println("arr1 is", arr1) // [1 4 7 2]
	fmt.Println("arr2 is", arr2) // [1 4 90 2]
}
