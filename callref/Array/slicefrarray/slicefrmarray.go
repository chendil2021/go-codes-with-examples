package main

import "fmt"

func main() {
	var arrayval [10]int = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	slice1 := arrayval[3:7]
	slice2 := arrayval[:6]
	slice3 := arrayval[5:]
	slice4 := arrayval[:]
	fmt.Println("slice from array:", slice1)
	fmt.Println("slice with default low indice:", slice2)
	fmt.Println("slice with default high indice:", slice3)
	fmt.Println("slice with default low and high indice:", slice4)
}
