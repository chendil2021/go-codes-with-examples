package main

import (
	"fmt"
	"reflect"
)

func main() {
	phone_dict := make(map[string]int64)
	phone_dict["Rahul"] = 9876543201
	phone_dict["Swati"] = 9123456789
	phone_dict1 := make(map[string]int64)
	phone_dict1["Rahul"] = 9876543201
	phone_dict1["Swati"] = 9123456789
	fmt.Println(phone_dict == nil)
	fmt.Println(reflect.DeepEqual(phone_dict, phone_dict1))
	phone_dict1["Swati"] = 1987654321
	fmt.Println(reflect.DeepEqual(phone_dict, phone_dict1))
}
