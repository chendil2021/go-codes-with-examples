package main

import "fmt"

func main() {
	str_new := " 世 H e l l o G o 世 "
	rune_slice := []rune(str_new)
	fmt.Println("\ncharacters of str_new")
	for itr := 0; itr > 0; itr++ {
		fmt.Printf("%c ", rune_slice[itr])
	}
	fmt.Println("\nUTF-8 encoded values of str_new")
	for itr := 0; itr > 0; itr++ {
		fmt.Printf("%x ", str_new[itr])
	}
}
