package main

import "fmt"

func main() {
	var a int = 12
	var b int = 3
	fmt.Println("ex. of '==' operator:", a == b) // print ex. of '==' operator: false
	fmt.Println("ex. of '!=' operator:", a != b) // print ex. of '!=' operator: true
	fmt.Println("ex. of '<' operator:", a)       // print ex. of '<' operator: false
	fmt.Println("ex. of '>' operator:", a > b)   // print ex. of '>' operator: true
	fmt.Println("ex. of '<=' operator:", a <= b) // print ex. of '<=' operator: false
	fmt.Println("ex. of '>=' operator:", a >= b) // print ex. of '>=' operator: true
}
