package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func square(value chan<- int) {
	fmt.Println("defining send only goroutine")
	val := value
	fmt.Println(val)
	wg.Done()
}
func main() {
	send_ch := make(chan<- int, 1)
	wg.Add(1)
	go square(send_ch)
	send_ch <- 3
	wg.Wait()
}
