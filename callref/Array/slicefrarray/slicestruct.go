package main

import "fmt"

func main() {
	var arrayval [10]int = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	slice1 := arrayval[1:8]
	slice2 := slice1[:6]
	fmt.Println("original array", arrayval)
	fmt.Println("slice from array:", slice1)
	fmt.Println("slice from existing slice", slice2)
	slice1[0] = 178
	fmt.Println("array after modifying slice", arrayval)
	fmt.Println("slice from array after modifying slice:", slice1)
	fmt.Println("slice from existing slice after modifying slice", slice2)
}
