package main

import "fmt"

func square(num_ch chan int) {
	value := <-num_ch
	sqr_val := value * value
	num_ch <- sqr_val
}
func cube(num_ch chan int) {
	value := <-num_ch
	cube_val := value * value * value
	num_ch <- cube_val
}
func main() {
	num := make(chan int)                    // 1
	go square(num)                           // 2
	num <- 3                                 // 3
	sqr_val := <-num                         // 4
	go cube(num)                             // 5
	num <- 3                                 // 6
	cube_val := <-num                        // 7
	fmt.Println("Square of value:", sqr_val) // 8
	fmt.Println("Cube of value:", cube_val)  // 9
}
