package main

import (
	"fmt"
	"time"
)

func show_value(i int) {
	for i < 10 {
		fmt.Println(i)
		time.Sleep(200 * time.Millisecond)
		i = i + 1
	}
}
func show_value1(c int) {
	for c < 110 {
		fmt.Println(c)
		time.Sleep(200 * time.Millisecond)
		c = c + 1
	}
}
func main() {
	go show_value(0)
	fmt.Println("We have called a goroutine show_value")
	go show_value1(100)
	fmt.Println("We have called a goroutine show_value1")
	fmt.Scanln()
}
