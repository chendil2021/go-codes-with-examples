package main

import "fmt"

type AirthOperation interface {
	Add() float32
}
type CompareOperation interface {
	Compare() bool
}
type Operation interface {
	AirthOperation
	Compare() bool
}
type Operandset1 struct {
	op1 float32
	op2 float32
}

func (operandset1_var Operandset1) Add() float32 {
	return operandset1_var.op1 + operandset1_var.op2
}
func (operandset1_var Operandset1) Compare() bool {
	return operandset1_var.op1 == operandset1_var.op2
}
func main() {
	var Operation_var Operation
	var CompareOperation_var CompareOperation
	var AirthOperation_var AirthOperation
	Operation_var = Operandset1{5.78, 6.8}
	fmt.Println(Operation_var, Operation_var.Add(), Operation_var.Compare())
	CompareOperation_var = Operandset1{6.8, 6.8}
	fmt.Println(CompareOperation_var, CompareOperation_var.Compare())
	AirthOperation_var = Operandset1{5, 7.8}
	fmt.Println(AirthOperation_var, AirthOperation_var.Add())
}
