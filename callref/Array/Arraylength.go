package main

import "fmt"

func main() {
	arr1D := []int{1, 4, 7, 2}
	arr2D := [3][2]int{
		{1, 2},
		{7},
	}
	fmt.Println("Length of 1D array", len(arr1D))
	fmt.Println("Length of 2D array", len(arr2D)) //tells number of rows it can have.
}
