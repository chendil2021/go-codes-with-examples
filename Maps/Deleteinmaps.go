package main

import (
	"fmt"
)

func main() {
	phone_dict3 := make(map[string]int64)
	phone_dict3["Rahul"] = 9876543201
	phone_dict3["Swati"] = 9123456789
	phone_dict3["Suman"] = 9675463768
	phone_dict3["Mayank"] = 9342516789
	phone_dict3["Deepak"] = 8765453678
	fmt.Println("Before deleting a key:", phone_dict3)
	delete(phone_dict3, "Suman")
	fmt.Println("After deleting a key:", phone_dict3)
}
