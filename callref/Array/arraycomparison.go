package main

import "fmt"

func main() {
	arr1 := [4]int{1, 4, 7, 2}
	arr2 := [4]int{1, 4, 7}
	arr3 := [3][2]int{
		{1, 2},
		{7},
	}
	arr4 := [3][2]int{
		{1, 1},
		{7},
	}
	arr5 := [4]int{1, 4, 7, 0}
	fmt.Println("arr1 and arr2", arr1 == arr2)
	fmt.Println("arr3 and arr4", arr3 == arr4)
	fmt.Println("arr2 and arr5", arr2 == arr5)
}
