package main

import "fmt"

func main() {
	ch := make(chan int, 2)
	cap_ch := cap(ch)
	fmt.Println("Capacity of channel is:", cap_ch)
	for itr := 1; itr <= cap_ch; itr++ {
		ch <- itr * 2
		fmt.Println("value received from channel is:", <-ch)
	}
}
