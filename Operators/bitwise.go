package main

import "fmt"

func main() {
	var a int = 50
	var b int = 6
	fmt.Println("a&b is:", a&b)   //prints a&b is: 2
	fmt.Println("a|b is:", a|b)   //prints a|b is: 54
	fmt.Println("a^b is:", a^b)   //prints a^b is: 52
	fmt.Println("a< is:", b<<2)   //prints a< is: 24
	fmt.Println("a>>b is:", a>>5) //prints a>>b is: 1
}
