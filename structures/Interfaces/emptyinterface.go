package main

import "fmt"

type empty_interface interface {
}

func show_value(interface_var empty_interface) {
	fmt.Println(interface_var)
}
func main() {
	type string_struct struct {
		a string
	}
	string_struct_var := string_struct{"Hello world"}
	type float_struct struct {
		a float32
	}
	float_struct_var := float_struct{77.88}
	show_value(string_struct_var) // {Hello world}
	show_value(float_struct_var)  // {77.88}
}
