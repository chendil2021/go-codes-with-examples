package main

import (
	"fmt"
	"reflect"
	"unsafe"
)

func main() {
	var a int64 = 100
	fmt.Println("size of variable a:", unsafe.Sizeof(a))         //prints 8
	fmt.Println("size of variable a:", reflect.TypeOf(a).Size()) //prints 8
	fmt.Println("address of variable a:", &a)                    //prints Address of variable a: 0x1184e068
	fmt.Println("value of a at specified address:", *(&a))       //prints Address of variable a: 100
}
