package main

import "fmt"

type AirthOperation interface {
	Add() float32
	Subtract(float32, float32) float32
	Multiply(int, int) int
}
type Operandset1 struct {
	op1 float32
	op2 float32
}

func (operandset1_var Operandset1) Add() float32 {
	return operandset1_var.op1 + operandset1_var.op2
}
func (operandset1_var Operandset1) Subtract(a float32, b float32) float32 {
	return a - b
}
func (operandset1_var Operandset1) Multiply(a float32, b float32) float32 {
	return a * b
}

type Operandset2 struct {
	op1 int
	op2 int
}

func (operandset2_var Operandset2) Add(a int, b int) int {
	return int(a + b)
}
func (operandset2_var Operandset2) Subtract(a int, b int) int {
	return (a - b)
}
func (operandset2_var Operandset2) Multiply(a int, b int) int {
	return (a * b)
}
func main() {
	operandset1_var := Operandset1{3.6, 6.9}
	operandset2_var := Operandset2{45, 67}
	fmt.Println("Add method with Operandset1:", operandset1_var.Add())
	fmt.Println("Subtract method with Operandset1:", operandset1_var.Subtract(operandset1_var.op1, operandset1_var.op2))
	fmt.Println("Multiply method with Operandset1:", operandset1_var.Multiply(operandset1_var.op1, operandset1_var.op2))
	fmt.Println("Add method with Operandset2:", operandset2_var.Add(operandset2_var.op1, operandset2_var.op2))
	fmt.Println("Subtract method with Operandset2:", operandset2_var.Subtract(operandset2_var.op1, operandset2_var.op2))
	fmt.Println("Multiply method with Operandset2:", operandset2_var.Multiply(operandset2_var.op1, operandset2_var.op2))
}
