package main

import (
	"fmt"
	"reflect"
)

func main() {
	phone_dict3 := make(map[string]int64)
	phone_dict3["Rahul"] = 9876543201
	phone_dict3["Swati"] = 9123456789
	mapkeys := reflect.ValueOf(phone_dict3).MapKeys()
	fmt.Println("Keys of phone_dict3", mapkeys)
	val, ok := phone_dict3["Swati"]
	fmt.Println(ok, val)
	val, ok = phone_dict3["Suman"]
	fmt.Println(ok, val)
}
