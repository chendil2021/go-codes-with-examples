package main

import "fmt"

func main() {
	phone_dict1 := map[string]int64{}
	phone_dict1["Rahul"] = 9876543201
	fmt.Println(phone_dict1)
	phone_dict2 := map[string]int64{
		"Rahul": 9876543201,
		"Swati": 9123456789,
	}
	phone_dict2["aayush"] = 9823165748
	fmt.Println(phone_dict2["Swati"])
	phone_dict3 := make(map[string]int64)
	fmt.Println(phone_dict3) // map[]
	phone_dict3["Rahul"] = 9876543201
	phone_dict3["Swati"] = 9123456789
	fmt.Println(phone_dict3)
	fmt.Println(phone_dict3["aayush"])
}
