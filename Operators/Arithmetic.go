package main

import "fmt"

func main() {
	var a int = 12
	var b int = 3
	fmt.Println("ex. of '+' operator:", a+b) // print “ex. of '+' operator: 15”
	fmt.Println("ex. of '-' operator:", a-b) // print “ex. of '-' operator: 9”
	fmt.Println("ex. of '*' operator:", a*b) // print “ex. of '*' operator: 36”
	fmt.Println("ex. of '/' operator:", a/b) // print “ex. of '/' operator: 4”
	fmt.Println("ex. of '%' operator:", a%b) // print “ex. of '%' operator: 0”
	a++
	fmt.Println("ex. of '++' operator:", a) // print “ex. of '++' operator: 13”
	b--
	fmt.Println("ex. of '--' operator:", b) // print “ex. of '--' operator: 2”
}
