package main

import "fmt"

func main() {
	var a int = 100
	switch a {
	case 20:
		fmt.Println("value of a is 20")
	case 50:
		fmt.Println("value of a is 50")
	case 100:
		fmt.Println("value of a is 100")
	default:
		fmt.Println("switch loop processed")
	}
}
