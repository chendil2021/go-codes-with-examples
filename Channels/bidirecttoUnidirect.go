package main

import "fmt"

func receive(receive_ch chan<- int, val int) {
	receive_ch <- val
}
func square(receive_ch <-chan int, send_ch chan<- int) {
	fmt.Println("defining send only goroutine")
	val := <-receive_ch
	send_ch <- val
}
func main() {
	send_ch := make(chan int, 1)
	receive_ch := make(chan int, 1)
	receive(receive_ch, 6)
	square(receive_ch, send_ch)
	fmt.Println(<-send_ch)
}
