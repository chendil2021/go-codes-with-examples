package main

import "fmt"

type AirthOperation interface {
	Add() float32
}
type Operandset1 struct {
	op1 float32
	op2 float32
}
type Operandset2 struct {
	op1 int
	op2 int
}

func (operandset1_var Operandset1) Add() float32 {
	return operandset1_var.op1 + operandset1_var.op2
}
func (operandset2_var Operandset2) Add() float32 {
	return float32(operandset2_var.op1 + operandset2_var.op2)
}
func main() {
	var interface_var AirthOperation
	interface_var = Operandset1{5.78, 6.8}
	fmt.Println(interface_var.Add()) // 12.58
	interface_var = Operandset2{5, 6}
	fmt.Println(interface_var.Add()) //11
}
