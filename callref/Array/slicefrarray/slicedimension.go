package main

import "fmt"

func main() {
	slice1 := [][]int{
		[]int{1, 2, 78, 54},
		[]int{0, 3, 4},
		[]int{5, 6},
	}
	fmt.Println("slice1", slice1) // slice1 [[1 2 78 54] [0 3 4] [5 6]]
	slice2 := make([][]string, 4)
	slice2[0] = []string{"College", "School"}
	slice2[1] = []string{"University", "Institute"}
	fmt.Println("slice2 using make function:", slice2) // slice2 using make function: [[College School] [University Institute] [] []]
}
