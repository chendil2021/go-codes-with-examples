package main

import "fmt"

func cube(value chan int) {
	fmt.Println("In cube goroutine")
	val := <-value
	value <- val * val * val
}
func square(value chan int) {
	fmt.Println("In square goroutine")
	val := <-value
	fmt.Println(val)
	value <- val * val
}
func main() {
	num := make(chan int)
	go square(num)
	num <- 3
	sqr_val := <-num
	go cube(num)
	num <- 3
	close(num)
	cube_val, ok := <-num
	fmt.Println("Square of value:", sqr_val)
	fmt.Println("Cube of value:", cube_val, ok)
}
