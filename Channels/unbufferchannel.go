package main

import "fmt"

func main() {
	ch := make(chan int, 2)
	ch <- 8
	a := <-ch
	fmt.Println(ch, a)
}
