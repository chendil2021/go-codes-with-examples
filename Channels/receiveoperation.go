package main

import "fmt"

func show_value(c chan int) {
	c <- 8
}
func main() {
	c := make(chan int)
	go show_value(c)
	a, ok := <-c
	fmt.Println("value received from channel:", a)
	fmt.Println("status of operation is:", ok)
}
