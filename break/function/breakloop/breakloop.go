package main

import "fmt"

func main() {
	var a int = 0
	for a < 20 {
		fmt.Printf("value of a: %d\n", a)
		a++
		if a == 5 {
			/* terminate the loop using break statement */
			break
		}
	}
}
