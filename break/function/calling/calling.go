package main
 import "fmt"
 func sum(a, b int, c string) (int, string){
 sum_value := a+b
 return sum_value, c
 }
 func main() {
 a := 90
 b := 30
 var c string
 result, c := sum(a, b, "Hello Go")
 fmt.Println("Values returned from the function are: ", result, c)
 }
