package main

import "fmt"

type AirthOperation interface {
	Add() float32
}
type CompareOperation interface {
	Compare(float32, float32) bool
}
type Operandset1 struct {
	op1 float32
	op2 float32
}

func (operandset1_var Operandset1) Add() float32 {
	return operandset1_var.op1 + operandset1_var.op2
}
func (operandset1_var Operandset1) Compare(a float32, b float32) bool {
	return a == b
}
func main() {
	operandset1_var := Operandset1{3.6, 6.9}
	fmt.Println(operandset1_var.Add())                                             // 10.5
	fmt.Println(operandset1_var.Compare(operandset1_var.op1, operandset1_var.op2)) // false
	var AirthOperation_var AirthOperation = operandset1_var
	fmt.Println(AirthOperation_var.Add()) // 10.5
	var compareOperation_var CompareOperation = operandset1_var
	fmt.Println(compareOperation_var.Compare(operandset1_var.op1, operandset1_var.op2)) // false
}
