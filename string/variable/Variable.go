var variable1 int //declare int type variable "variable1"
 fmt.Println(variable1) //print value 0
 var variable2 string
 fmt.Println(variable2) //print ""
 variable1 = 123
 variable2 = "hello world"
 fmt.Println(variable1, variable2) //print 123 hello world
 var variable3 = 123.45
 fmt.Println(variable3) //print 123.45
 variable4 := false
 fmt.Println(variable4) //print false
 variable5, variable6 := "multiple", 2
 fmt.Println(variable5, variable6) //print multiple 2
 var variable7, variable8, variable9 int //declaring multiple variables of same type
 variable7, variable8, variable9 = 123, 456, 789
 fmt.Println(variable7, variable8, variable9) //print 123 456 789
 var(
 variable10 int
 variable11 string
 variable12 bool
 ) //declaring multiple variables of different type at the same type.
 fmt.Println(variable10, variable11, variable12) //print 0 false
 variable10, variable11, variable12 = 123, "multiple", true
 fmt.Println(variable10, variable11, variable12) //print 123 multiple true
 variable13, variable14 := 123, 456
 fmt.Println(variable13) //error "variable14 declared and not used"
 variable15, _:= 123, 456 //use of "blank identifier"
fmt.Println(variable15) //print 123
