package main
 import "fmt"
 func main() {
 var str = "hello"
 var intval = 78
 var floatval = 56.8789
 var complexval = 67+6i
 var boolval = true
 var pointerval = &str
 fmt.Printf("example of %%v, value is: %v \n", intval)
 fmt.Printf("example of %%#v, value is: %#v \n", str)
 fmt.Printf("example of %%T, value is: %T \n", intval)
 fmt.Printf("example of %%, used to print %%\n\n")
 fmt.Printf("example of %%t, value is: %t\n\n", boolval)
 fmt.Printf("example of %%b, value is: %b\n", intval)
 fmt.Printf("example of %%c, value is: %c\n", intval)
 fmt.Printf("example of %%d, value is: %d\n", intval)
 fmt.Printf("example of %%o, value is: %o\n", intval)
 fmt.Printf("example of %%O, value is: %O\n", intval)
 fmt.Printf("example of %%q, value is: %q\n", intval)
 fmt.Printf("example of %%x, value is: %x\n", intval)
 fmt.Printf("example of %%X, value is: %X\n", intval)
 fmt.Printf("example of %%U, value is: %U\n\n", intval)
 fmt.Printf("example of %%b, value is: %b\n", floatval)
 fmt.Printf("example of %%e, value is: %e\n", floatval)
 fmt.Printf("example of %%E, value is: %E\n", complexval)
 fmt.Printf("example of %%f, value is: %f\n", floatval)
 fmt.Printf("example of %%F, value is: %F\n", floatval)
 fmt.Printf("example of %%g, value is: %g\n", floatval)
 fmt.Printf("example of %%G, value is: %G\n", floatval)
 fmt.Printf("example of %%x, value is: %x\n", floatval)
 fmt.Printf("example of %%X, value is: %X\n\n", floatval)
 fmt.Printf("example of %%s, value is: %s\n", str)
 fmt.Printf("example of %%q, value is: %q\n", str)
 fmt.Printf("example of %%x, value is: %x\n", str)
 fmt.Printf("example of %%X, value is: %X\n\n", str)
 fmt.Printf("example of %%p, value is: %p \n", pointerval)
 }
