package main

import (
	"fmt"
	"sort"
)

func main() {
	phone_dict3 := make(map[string]int64)
	phone_dict3["Rahul"] = 9876543201
	phone_dict3["Swati"] = 9123456789
	phone_dict3["Suman"] = 9675463768
	phone_dict3["Mayank"] = 9342516789
	phone_dict3["Deepak"] = 8765453678
	var mapkeys []string
	for itr := range phone_dict3 {
		fmt.Println(itr)
		mapkeys = append(mapkeys, itr)
	}
	sort.Strings(mapkeys)
	for _, k := range mapkeys {
		fmt.Println("Key:", k, "Value:", phone_dict3[k])
	}
}
