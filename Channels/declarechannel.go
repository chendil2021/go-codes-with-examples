package main

import "fmt"

func main() {
	var ch1 chan int
	fmt.Println("channel with 'var' keyword:", ch1)
	ch2 := make(chan string)
	fmt.Println("channel with 'make' method:", ch2)
}
