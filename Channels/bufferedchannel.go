package main

import "fmt"

func main() {
	ch := make(chan int, 2)
	ch <- 8
	ch <- 99
	a := <-ch
	b := <-ch
	fmt.Println(a, b)
}
