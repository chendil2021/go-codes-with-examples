package main

import "fmt"

func main() {
	phone_dict := make(map[string]int64)
	phone_dict["Rahul"] = 9876543201
	phone_dict["Swati"] = 9123456789
	phone_dict["Suman"] = 9675463768
	phone_dict["Mayank"] = 9342516789
	phone_dict["Deepak"] = 8765453678
	fmt.Println(phone_dict["Mayank"])
	phone_dict1 := phone_dict
	phone_dict1["Mayank"] = 1234567890
	fmt.Println(phone_dict["Mayank"])
}
