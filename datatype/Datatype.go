var int32val int32 = 100
var int valint = 76 //treats as int32 or int64 depends on implementation
fmt.Println(int32val, intval)
var int16val int16 = int64val // error “cannot use int32val int32> as type int16 in assignment”
var int16val int16 = int16(int32val)
fmt.Println(int16val)         // print 100
var int64val int64 = int32val // error “cannot use int32val int32> as type int64 in assignment”
var int64val int64 = int64(int32val)
fmt.Println(int64val)         //print 100
var uint8val uint8 = int32val // error “cannot use int32val int32> as type uint8 in assignment”
var uint8val uint8 = uint8(int32val)
fmt.Println(uint8val) // print 100
int32val = -100
uint8val = uint8(int32val)
fmt.Println(uint8val) // print 156, as -100 is converting to unsigned integer (using 2’s complement)
