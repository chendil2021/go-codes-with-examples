package main

import (
	"fmt"
	"time"
)

func square(num_ch chan int) {
	value := <-num_ch
	sqr_val := value * value
	time.Sleep(5 * time.Second)
	num_ch <- sqr_val
}
func cube(num_ch chan int) {
	value := <-num_ch
	cube_val := value * value * value
	time.Sleep(10 * time.Second)
	num_ch <- cube_val
}
func main() {
	sqr_ch := make(chan int)
	go square(sqr_ch)
	sqr_ch <- 3
	cube_ch := make(chan int)
	go cube(cube_ch)
	cube_ch <- 5
	select {
	case sqr_val := <-sqr_ch:
		fmt.Println("square of a value is:", sqr_val)
	case cube_val := <-cube_ch:
		fmt.Println("Cube of a value is:", cube_val)
	}
}
