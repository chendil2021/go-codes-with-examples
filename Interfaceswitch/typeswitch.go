package main

import "fmt"

type AirthOperation interface {
	Add() float32
}
type Operandset1 struct {
	op1 float32
	op2 float32
}
type Operandset2 struct {
	op1 int
	op2 int
}
type Operandset3 struct {
	op1 string
	op2 string
}

func main() {
	check(Operandset1{3.7, 6.7})
	check(Operandset2{37, 67})
	check(Operandset3{"abc", "def"})
}
func check(i interface{}) {
	switch v := i.(type) {
	case Operandset1:
		fmt.Println("Interface has a value of type Operandset1 and the value is", v)
	case Operandset2:
		fmt.Println("Interface has a value of type Operandset2 and the value is", v)
	default:
		fmt.Println("Default case, Interface doesn't have a value of type", v)
	}
}
